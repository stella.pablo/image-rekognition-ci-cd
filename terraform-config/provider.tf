provider "aws" {
  region = var.aws_region
}

terraform {
  backend "s3" {
    bucket = "moebius00"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}